function isElementInViewport(element) {
    var bRect = element[0].getBoundingClientRect();

    return (
        bRect.bottom >= 0 &&
        bRect.top <= (window.innerHeight || $(window).height()) &&
        bRect.left >= 0 &&
        bRect.right <= (window.innerWidth || $(window).width()));
}


function checkAnimation() {
    var $elements = $(".anim");

    $elements.each(function() {
        var $element = $(this)

        if ($element.hasClass("anim-start")) {
            return;
        }

        if (isElementInViewport($element)) {
            // Start the animation
            $element.addClass("anim-start");
        }
    });
}

$(window).scroll(function(){
    checkAnimation();
});
